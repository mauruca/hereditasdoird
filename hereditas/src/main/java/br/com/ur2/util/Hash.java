/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by mauricio on 19/05/16.
 */
public class Hash {
    public static String DoSHA512(String valor, String salt, int maxLength)
    {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes("UTF-8"));
            byte[] bytes = md.digest(valor.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++){
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e){
            Erro.Registra(e);
            return null;
        }
        catch (UnsupportedEncodingException ec)
        {
            Erro.Registra(ec);
            return null;
        }
        // Validando assertivas de saida
        if(generatedPassword == null)
            return null;
        if(generatedPassword.length() == 0)
            return "";
        if(generatedPassword.length() < maxLength)
            maxLength = generatedPassword.length();
        return generatedPassword.substring(0,maxLength);
    }
}
