/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import br.com.ur2.util.Erro;

/**
 * Created by mauricio on 05/06/16.
 */
public class EmbaixadorBT {

    // ---
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothDevice mBluetoothDevice;
    private BluetoothSocket mBluetoothSocket;
    private UUID mUuid;

    private Handler eventosBT;

    private ConnectThread mConnectThread;
    private ConnectedThread mConnectedThread;

    public EmbaixadorBT(UUID uuid)
    {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mUuid = uuid;
    }

    public void pararDescobertaDispositivos()
    {
        if( mBluetoothAdapter != null && mBluetoothAdapter.isDiscovering() )
            mBluetoothAdapter.cancelDiscovery();
    }

    public void connectToBluetoothDevice( BluetoothDevice device, UUID uuid )
    {
        mBluetoothDevice = device;
        mUuid = uuid;
        disconnect();
        mConnectThread = new ConnectThread();
        mConnectThread.start();
    }

    public void disconnect()
    {
        // ----------
        try
        {
            if( mConnectThread != null )
            {
                mConnectThread.cancel();
                mConnectThread.interrupt();
            }
        }
        catch( Exception ex )
        {
            Erro.Registra(ex);
        }
        mConnectThread = null;

        // ----------
        try
        {
            if( mConnectedThread != null )
            {
                mConnectedThread.cancel();
                mConnectedThread.interrupt();
            }
        }
        catch( Exception ex )
        {
            Erro.Registra(ex);
        }
        mConnectedThread = null;
    }

    // ------------- For Connecting to Bluetooth
    private class ConnectThread extends Thread
    {
        public ConnectThread()
        {
            if( mBluetoothDevice == null ) {
                MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoFalharConexao,"Dispositivo não criado.");
                return;
            }

            try
            {
                mBluetoothSocket = mBluetoothDevice.createRfcommSocketToServiceRecord(mUuid);
            }
            catch( IOException e )
            {
                Erro.Registra(e);
                MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoFalharConexao,"Erro ao tentar criar socket.");
            }
        }

        public void run()
        {
            pararDescobertaDispositivos();

            if( mBluetoothSocket == null )
            {
                MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoFalharConexao,"Socket está nulo para o dispositivo " + mBluetoothDevice.getName() + ".");
                return;
            }

            try
            {
                mBluetoothSocket.connect();
            }
            catch( IOException connectException )
            {
                try
                {
                    mBluetoothSocket.close();
                }
                catch( IOException closeException )
                {
                    Erro.Registra(closeException);
                    MensagemBT.enviaMensagem(eventosBT, MensagemBT.aoFalharConexao, "Erro ao fechar socket.");
                }
                Erro.Registra(connectException);
                MensagemBT.enviaMensagem(eventosBT, MensagemBT.aoFalharConexao, "Erro ao conectar ao socket.");

                mConnectThread = null;
                return;
            }

            // manageConnectedSocket(mmSocket);
            mConnectedThread = new ConnectedThread(mBluetoothSocket);
            if( mConnectedThread.isInitOk() )
            {
                mConnectedThread.start();
                MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoConectar);
            }


            mConnectThread = null;
        }

        public void cancel()
        {
            try {
                if(mBluetoothSocket.isConnected())
                    mBluetoothSocket.close();
                MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoDesconectar);

            } catch (IOException e) {
                Erro.Registra(e);
            }
        }
    };


    // ------------- For Manage bluetooth
    private class ConnectedThread extends Thread
    {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private boolean mInitOk;

        public ConnectedThread( BluetoothSocket socket )
        {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;
            mInitOk = false;
            try
            {
                tmpIn = socket.getInputStream();
                tmpOut = socket.getOutputStream();
                mInitOk = true;
            }
            catch( IOException e )
            {
                Erro.Registra(e);
                MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoFalharConexao);
                mInitOk = false;
            }
            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public boolean isInitOk()
        {
            return mInitOk;
        }

        public void run()
        {
            byte[] buffer = new byte[1024];
            int bytes;
            while( mInitOk )
            {
                try
                {
                    bytes = mmInStream.read(buffer);

                    // BluetoothDevice device = mmSocket.getRemoteDevice();

                    trataPacote(buffer, bytes);

                    //if( mBtEventListener != null )
                    //    mBtEventListener.onNotifyBtDataRecv();
                    MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoEnviarDadosDispositivo);
                }
                catch( IOException e )
                {
                    Erro.Registra(e);
                    MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoLevantarExcecaoTransmitirDados,"Falha ao ler do socket.");
                    break;
                }
            }
        }

        /* Call this from the main Activity to send data to the remote device */
        public void write( byte[] bytes )
        {
            try
            {
                mmOutStream.write(bytes);
                MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoEnviarDadosDispositivo);
            }
            catch( IOException e )
            {
                Erro.Registra(e);
                MensagemBT.enviaMensagem(eventosBT,MensagemBT.aoLevantarExcecaoTransmitirDados,"Falha ao escrever no socket.");
            }
        }

        /* Call this from the main Activity to shutdown the connection */
        public void cancel()
        {
            try
            {
                mmSocket.close();
            }
            catch( IOException e )
            {
                Erro.Registra(e);
            }
        }
    }

    private StringBuilder mPacket = new StringBuilder();
    private char[] mCharBuff = null;
    private int mCharBuffSize = 0;

    synchronized public void trataPacote(byte[] buffer, int len )
    {
        if( mCharBuffSize < len )
        {
            mCharBuffSize = ( len << 1 );
            mCharBuff = new char[ mCharBuffSize ];
        }

        for( int i = 0; i < len; ++i )
            mCharBuff[ i ] = (char)( buffer[ i ] & 0xff );
        mPacket.append( mCharBuff, 0, len );
    }
}
