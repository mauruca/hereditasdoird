/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;

import br.com.ur2.dispositivo.modelo.MensagemDispositivo;
import br.com.ur2.hereditas.adaptadores.BemArrayAdapter;
import br.com.ur2.hereditas.controlador.HereditasControlador;
import br.com.ur2.hereditas.controlador.InventarioDadosControlador;
import br.com.ur2.hereditas.modelo.Bem;
import br.com.ur2.hereditas.tarefas.ProcessaBensEtiquetasMemoriaAsyncTask;
import br.com.ur2.util.Facilitador;

public class InventarioActivity extends AppCompatActivity {

    ProcessaBensEtiquetasMemoriaAsyncTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inventario);

        configurar();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(!HereditasControlador.getInstance().usaRFID())
            findViewById(R.id.btScanBemInventario).setVisibility(View.VISIBLE);
        ativar();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(task != null)
            task.cancel(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            String re = scanResult.getContents();
            MensagemDispositivo.enviaMensagemHandler(HereditasControlador.getInstance().obtemDispositivo().obtemHandlerDispositivo(), MensagemDispositivo.aoReceberDados,re);
        }
    }

    private void configurar()
    {
        Object o;
        o = findViewById(R.id.lvInventarioBem);

        if(o == null)
            return;

        ListView lv = (ListView)o;

        // crio o adaptador para a lista de locais
        final BemArrayAdapter adapter = new BemArrayAdapter(this,
                android.R.layout.simple_list_item_1, new ArrayList<Bem>());

        lv.setAdapter(adapter);
    }

    private void ativar()
    {
        InventarioDadosControlador.getInstance().limparMemoriaBens();
        HereditasControlador.getInstance().obtemDispositivo().ativar(this);
        task = new ProcessaBensEtiquetasMemoriaAsyncTask() {
            @Override
            protected void onProgressUpdate(ArrayList<Bem>... values) {
                atualizaListaBens(values[0]);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void atualizaListaBens(List<Bem> lista)
    {
        if(lista == null)
            return;

        Object o;
        o = findViewById(R.id.lvInventarioBem);

        if(o == null)
            return;

        ListView lv = (ListView)o;
        ArrayAdapter<Bem> ad = ((BemArrayAdapter)lv.getAdapter());
        if(ad == null)
            return;

        ad.clear();
        ad.addAll(lista);
        ad.notifyDataSetChanged();
    }

    // Eventos de chamados pela interface
    public void scan(View view)
    {
        atualizaListaBens(new ArrayList<Bem>());
        HereditasControlador.getInstance().obtemDispositivo().obtemMemoria().limpa();
        HereditasControlador.getInstance().obtemDispositivo().ler(true,true);
    }

    public void RegistraInventario(View view)
    {
        task.cancel(true);
        HereditasControlador.getInstance().obtemDispositivo().parar();
        if(!InventarioDadosControlador.getInstance().adicionaInventario())
        {
            Facilitador.informa("Não existem dados ou inventariante autenticado não registrado para criar inventário",true);
            return;
        }
        Facilitador.informa("Inventario registrado",true);
    }

}
