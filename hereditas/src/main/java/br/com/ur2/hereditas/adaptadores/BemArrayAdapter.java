/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.ur2.hereditas.R;
import br.com.ur2.hereditas.modelo.Bem;

/**
 * Created by mauricio on 25/05/16.
 */
public class BemArrayAdapter extends ArrayAdapter<Bem> {

    ArrayList<Bem> original;

    public BemArrayAdapter(Context context, int textViewResourceId,
                           List<Bem> objects) {
        super(context, textViewResourceId, objects);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListaViewHolder holder;
        Bem bem = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_bens,parent,false);
        }

        holder = new ListaViewHolder();
        holder.text = (TextView) convertView.findViewById(R.id.tvDescBem);
        holder.status = (ImageButton) convertView.findViewById(R.id.ibBemRfidStatus);

        if(holder.isValid())
        {
            holder.text.setText(bem.descricao);
            holder.status.setBackgroundResource(bem.obtemStatusId());
        }
        else
            holder.text.setText("Erro ao ler item");

        convertView.setTag(holder);

        return convertView;
    }
}
