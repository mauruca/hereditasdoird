/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.modelo;

import br.com.ur2.hereditas.R;

/**
 * Created by mauricio on 26/05/16.
 */
public abstract class Base {

    public static final int EstadoRFID_OK = 0;
    public static final int EstadoRFID_Sincronizar = 1;
    public static final int EstadoRFID_Vazio = 2;

    public String rfid = "";
    private int estado = EstadoRFID_Vazio;

    public Base()
    {}

    public Base(String prfid)
    {
        this.rfid = prfid;
    }

    public int obtemEstado() {
        return estado;
    }

    public boolean valido()
    {
        return !(rfid == null || rfid.trim().length() == 0 && (estado != EstadoRFID_Vazio));
    }

    public void ajustaStatus()
    {
        if(rfid == null || rfid.trim().length() == 0) {
            estado = EstadoRFID_Vazio;
            return;
        }
        estado = EstadoRFID_Sincronizar;
    }

    public void atribuiEstadoOK()
    {
        estado = EstadoRFID_OK;
    }

    public void atribuiEstadoSincronizar() {
        estado = EstadoRFID_Sincronizar;
    }

    public int obtemStatusId()
    {
        switch (this.estado)
        {
            case EstadoRFID_OK:
                return R.mipmap.itemok;
            case EstadoRFID_Sincronizar:
                return R.mipmap.itemnaosincronizado;
            default:
                return R.mipmap.itemvazio;
        }
    }

    @Override
    public boolean equals(Object o) {
        return ((Base)o).rfid.equals(this.rfid);
    }
}
