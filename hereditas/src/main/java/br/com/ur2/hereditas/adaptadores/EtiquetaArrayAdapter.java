/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import br.com.ur2.hereditas.R;
import br.com.ur2.dispositivo.modelo.Tag;
import br.com.ur2.util.Facilitador;
import br.com.ur2.util.uLog;

/**
 * Created by mauricio on 25/05/16.
 */
public class EtiquetaArrayAdapter extends ArrayAdapter<Tag> {

    public EtiquetaArrayAdapter(Context context, int textViewResourceId,
                                List<Tag> objects) {
        super(context, textViewResourceId, objects);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        EtiquetaViewHolder holder;
        Tag tag = getItem(position);

        if(convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_tags,parent,false);
        }

        holder = new EtiquetaViewHolder();
        holder.text = (TextView) convertView.findViewById(R.id.tvTagId);
        holder.qt = (TextView) convertView.findViewById(R.id.tvTagQtd);
        holder.sinal = (ProgressBar) convertView.findViewById(R.id.pbSinal);

        if(holder.isValid())
        {
            holder.text.setText(tag.obtemId());
            holder.qt.setText(String.valueOf(tag.obtemQuantidade()));
            holder.sinal.setProgress(tag.forcaSinal());
        }
        else
            holder.text.setText("Erro ao ler item");

        convertView.setTag(holder);

        return convertView;
    }
}
