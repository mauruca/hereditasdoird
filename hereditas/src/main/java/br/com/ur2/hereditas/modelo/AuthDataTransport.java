/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.modelo;

import com.google.gson.Gson;

/**
 * Created by mauricio on 18/05/16.
 */
public class AuthDataTransport {
    public String username = "";
    public String password = "";

    public AuthDataTransport(String un, String pw)
    {
        username = un;
        password = pw;
    }

    public String Serializar()
    {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
