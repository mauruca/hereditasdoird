/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.modelo;

import com.google.gson.Gson;

/**
 * Created by mauricio on 25/05/16.
 */
public class InventarioDataTransport {
    public String data;
    public String bem;
    public String local;
    public String user;

    public InventarioDataTransport(String pdt, String pb, String pl, String pu)
    {
        data = pdt;
        bem = pb;
        local = pl;
        user = pu;
    }

    public String Serializar()
    {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
