package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import java.util.ArrayList;

import br.com.ur2.dispositivo.modelo.Tag;
import br.com.ur2.hereditas.controlador.HereditasControlador;
import br.com.ur2.util.Erro;

/**
 * Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
 * Ver arquivo LICENSE em www.hereditas.net.br para detalhes.
 * Created by mauricio on 30/06/16.
 */
public class AtualizaEtiquetasNaoUsadasDetalheAsyncTask extends AsyncTask<Void, ArrayList<Tag>, Void> {

    @Override
    protected Void doInBackground(Void... params) {

        while (true)
        {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                Erro.Registra(e);
            }
            ArrayList<Tag> lista = HereditasControlador.getInstance().buscaEtiquetasNaoUsadas(HereditasControlador.getInstance().obtemDispositivo().obtemMemoria());
            if (lista != null && lista.size() > 0)
                publishProgress(lista);
            // sai se cancelada
            if (isCancelled())
                break;
        }

        return null;
    }

}
