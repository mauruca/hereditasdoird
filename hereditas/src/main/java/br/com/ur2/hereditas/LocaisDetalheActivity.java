/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

import br.com.ur2.dispositivo.modelo.MensagemDispositivo;
import br.com.ur2.hereditas.adaptadores.EtiquetaArrayAdapter;
import br.com.ur2.hereditas.controlador.HereditasControlador;
import br.com.ur2.hereditas.controlador.LocalDadosControlador;
import br.com.ur2.dispositivo.modelo.Dispositivo;
import br.com.ur2.dispositivo.modelo.Tag;
import br.com.ur2.hereditas.tarefas.AtualizaEtiquetasNaoUsadasDetalheAsyncTask;
import br.com.ur2.util.Facilitador;

public class LocaisDetalheActivity extends AppCompatActivity {

    AtualizaEtiquetasNaoUsadasDetalheAsyncTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locais_detalhe);

        configura();

        if(HereditasControlador.getInstance().obtemDispositivo() != null)
        {
            if(HereditasControlador.getInstance().usaRFID())
                HereditasControlador.getInstance().obtemDispositivo().configuraFormaAtivacao(Dispositivo.FormaAtivacao.CONECTAR_E_LER_CONTINUO);
            else
                HereditasControlador.getInstance().obtemDispositivo().configuraFormaAtivacao(Dispositivo.FormaAtivacao.CONECTAR_E_LER);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        HereditasControlador.getInstance().obtemDispositivo().obtemMemoria().limpa();
        if(HereditasControlador.getInstance().usaRFID())
            HereditasControlador.getInstance().obtemDispositivo().ler(true,true);
        else
            findViewById(R.id.btScanLocal).setVisibility(View.VISIBLE);
        ativar();
    }

    @Override
    protected void onStop() {
        super.onStop();
        HereditasControlador.getInstance().obtemDispositivo().parar();
        if(task != null)
            task.cancel(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (scanResult != null) {
            String re = scanResult.getContents();
            MensagemDispositivo.enviaMensagemHandler(HereditasControlador.getInstance().obtemDispositivo().obtemHandlerDispositivo(), MensagemDispositivo.aoReceberDados,re);
        }
    }

    private void configura()
    {
        // Valida entrada na tela, por garantia das assertivas de entrada, pois antes a validação tb é feita na tela anterior
        if(HereditasControlador.getInstance().localSelecionadoEtiquetagem == null)
        {
            Facilitador.informa("Nenhum local foi selecionado.",true);
            finish();
            return;
        }
        ajustaDadosTela();


        ArrayList<Tag> lista = new ArrayList<Tag>();

        ListView lv = (ListView) findViewById(R.id.lvEtiquetasRFLocaisDetalhe);
        //
        if(lv == null)
            return;

        // crio o adaptador para a lista de tags
        final EtiquetaArrayAdapter adapter = new EtiquetaArrayAdapter(this,
                android.R.layout.simple_list_item_1, lista);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selecionaItemLista(parent.getAdapter().getItem(position));
            }
        });
    }

    private void ativar()
    {
        atualizarListaLocais(new ArrayList<Tag>());
        HereditasControlador.getInstance().obtemDispositivo().ativar(this);
        task = new AtualizaEtiquetasNaoUsadasDetalheAsyncTask() {
            @Override
            protected void onProgressUpdate(ArrayList<Tag>... values) {
                super.onProgressUpdate(values);
                atualizarListaLocais(values[0]);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void ajustaDadosTela()
    {
        Object o;
        o = findViewById(R.id.tvNomeLocalDetalhe);
        if(o==null)
            return;
        ((TextView)o).setText(HereditasControlador.getInstance().localSelecionadoEtiquetagem.nome);
        o = findViewById(R.id.tvEtiquetaLocalDetalhe);
        if(o==null)
            return;
        String rf = HereditasControlador.getInstance().localSelecionadoEtiquetagem.rfid;
        if(rf.length() == 0)
            return;
        ((TextView)o).setText(rf);
    }

    private void atualizarListaLocais(ArrayList<Tag> lista)
    {
        if(lista == null)
            return;

        Object o;
        o = findViewById(R.id.lvEtiquetasRFLocaisDetalhe);

        if(o == null)
            return;

        ListView lv = (ListView)o;
        ArrayAdapter<Tag> ad = ((EtiquetaArrayAdapter)lv.getAdapter());
        if(ad == null)
            return;

        ad.clear();
        ad.addAll(lista);
        ad.notifyDataSetChanged();
    }

    private void selecionaItemLista(Object o)
    {
        if(o == null)
            return;

        Tag t = ((Tag)o);

        LocalDadosControlador.getInstance().alteraRfidLocal(HereditasControlador.getInstance().localSelecionadoEtiquetagem.pk,t.obtemId());
        HereditasControlador.getInstance().localSelecionadoEtiquetagem = null;
        finish();
    }

    // Eventos de chamados pela interface
    public void scan(View view)
    {
        HereditasControlador.getInstance().obtemDispositivo().obtemMemoria().limpa();
        HereditasControlador.getInstance().obtemDispositivo().ler(true,true);
    }
}
