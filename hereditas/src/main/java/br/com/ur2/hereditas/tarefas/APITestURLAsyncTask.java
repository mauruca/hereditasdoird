package br.com.ur2.hereditas.tarefas;

import android.content.Context;
import android.os.AsyncTask;

import java.io.IOException;

import br.com.ur2.hereditas.ResultadoAssincrono;
import br.com.ur2.restproxy.ClienteRESTProxy;

/**
 * Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
 * Ver arquivo LICENSE em www.hereditas.net.br para detalhes.
 * Created by mauricio on 19/05/16.
 */
public class APITestURLAsyncTask extends AsyncTask<String, Void, Boolean> {
    Context ctx;

    private ResultadoAssincrono delegateResultado = null;

    public APITestURLAsyncTask(ResultadoAssincrono delegate) {
        delegateResultado = delegate;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        try
        {
            ClienteRESTProxy client = new ClienteRESTProxy(params[0],"");
            client.method = ClienteRESTProxy.HttpVerb.GET;
            String retorno = client.solicitacao(false);
            return retorno.length() > 0;
        }
        catch (IOException e)
        {
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean retorno) {
        super.onPostExecute(retorno);
        if(delegateResultado != null)
            delegateResultado.AoFinalizarTarefaAssincrona(retorno);
    }
}
