/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas;

/**
 * Created by mauricio on 23/05/16.
 */
public interface ResultadoAssincrono {
    void AoFinalizarTarefaAssincrona();
    void AoFinalizarTarefaAssincrona(Boolean bol);
    void AoFinalizarTarefaAssincrona(String msg);
}
