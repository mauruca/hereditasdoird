/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import br.com.ur2.hereditas.adaptadores.LocalArrayAdapter;
import br.com.ur2.hereditas.controlador.HereditasControlador;
import br.com.ur2.hereditas.controlador.LocalDadosControlador;
import br.com.ur2.hereditas.modelo.Local;
import br.com.ur2.util.Facilitador;

public class LocaisActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locais);

        configurar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        atualizaLista();
        if(HereditasControlador.getInstance().obtemDispositivo() != null)
        {
            HereditasControlador.getInstance().obtemDispositivo().parar();
        }
    }

    private void configurar()
    {
        ArrayList<Local> listaLocais = new ArrayList<>();

        ListView lv = (ListView) findViewById(R.id.lvEtiquetarLocal);
        //
        if(lv == null)
            return;

        // crio o adaptador para a lista de locais
        final LocalArrayAdapter adapter = new LocalArrayAdapter(this,
                android.R.layout.simple_list_item_1, listaLocais);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selecionaItemLista(parent.getAdapter().getItem(position));
            }
        });
    }

    private void selecionaItemLista(Object o)
    {
        if(o == null)
            return;

        HereditasControlador.getInstance().localSelecionadoEtiquetagem = ((Local)o);
        Intent intent = new Intent(this, LocaisDetalheActivity.class);
        startActivity(intent);
    }

    private void ativarDispositivo()
    {
        Facilitador.informa("Dispositivo inativo. Acionando ativação...",true);
        HereditasControlador.getInstance().obtemDispositivo().ativar(this);
    }

    private void atualizaLista()
    {
        Object o;
        o = findViewById(R.id.lvEtiquetarLocal);

        if(o == null)
            return;
        ListView lv = (ListView)o;

        ArrayList<Local> lista = LocalDadosControlador.getInstance().obtemListaArray();

        ((LocalArrayAdapter)lv.getAdapter()).clear();
        ((LocalArrayAdapter)lv.getAdapter()).addAll(lista);
        ((LocalArrayAdapter)lv.getAdapter()).notifyDataSetChanged();
    }

    // Mensagem Toast
    private void toast(String mensagem)
    {
        if(mensagem.trim().length() == 0)
            return;
        Toast.makeText(getBaseContext(), mensagem,Toast.LENGTH_SHORT).show();
    }
}
