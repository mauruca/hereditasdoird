/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.controlador;

import java.io.IOException;

import br.com.ur2.hereditas.ResultadoAssincrono;
import br.com.ur2.hereditas.constantes.API;
import br.com.ur2.hereditas.modelo.AuthDataTransport;
import br.com.ur2.hereditas.modelo.Token;
import br.com.ur2.hereditas.modelo.TokenDataTransport;
import br.com.ur2.hereditas.tarefas.APILoginAsyncTask;
import br.com.ur2.restproxy.ClienteRESTProxy;
import br.com.ur2.util.Erro;
import br.com.ur2.util.Facilitador;

/**
 * Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
 * Ver arquivo LICENSE em www.hereditas.net.br para detalhes.
 * Created by mauricio on 04/06/16.
 */
public class AutenticacaoTokenControlador {

    private static AutenticacaoTokenControlador instance = new AutenticacaoTokenControlador();
    private String chaveTokenId = "token";
    private Token token = null;

    public static AutenticacaoTokenControlador getInstance() {
        return instance;
    }

    public void definirToken(Token ptoken) {
        if (ptoken == null) {
            removeToken();
            return;
        }
        this.token = ptoken;
        Facilitador.definirPreferencia(chaveTokenId, this.token.chave());
    }

    public Token obtemToken() {
        return this.token;
    }

    public boolean tokenValido() {
        return (this.obtemToken() != null && this.obtemToken().valido());
    }

    public void configurar() {
        if (!tokenValido()) {
            String chaveToken = Facilitador.obterPreferenciaString(chaveTokenId);
            if (chaveToken.length() > 0)
                this.token = new Token(chaveToken);
        }
    }

    public void removeToken() {
        this.token = null;
        Facilitador.removerPreferencia(chaveTokenId);
    }

    public boolean validaTokenLicenca(String serial, String chave) {
        // validar assertivas de entrada
        if (serial.trim().length() == 0 || chave.trim().length() == 0) {
            throw new IllegalArgumentException("Serial e/ou Chave não podem ser vazios.");
        }

        Token tok = obtemTokenServidor(serial, chave);
        return tok != null && tok.valido();
    }

    public void verificaToken(String chave, String valor) {
        // validar assertivas de entrada
        if (chave.trim().length() == 0 || valor.trim().length() == 0) {
            throw new IllegalArgumentException("Chave e/ou Valor não podem ser vazios.");
        }
        // limpa token existente
        removeToken();
        // obtem novo token
        definirToken(obtemTokenServidor(chave, valor));
    }

    private Token obtemTokenServidor(String chave, String valor) throws IllegalArgumentException {
        try {
            ClienteRESTProxy client = new ClienteRESTProxy(HereditasControlador.getInstance().URL(), API.apiPathAuth);
            AuthDataTransport adt = new AuthDataTransport(chave, valor);
            client.postData = adt.Serializar();
            String retorno = client.solicitacao(false);
            if (retorno.length() == 0)
                return null;
            return TokenDataTransport.Deserializador(retorno);
        } catch (IOException e) {
            Erro.Registra(e);
            return null;
        }
    }

    public void ValidaUsuario(String usuario, String senha) throws IllegalArgumentException {

        // validar assertivas de entrada
        if (usuario.trim().length() == 0 || senha.trim().length() == 0) {
            throw new IllegalArgumentException("Usuário ou senha não podem ser vazios.");
        }
        verificaToken(usuario, senha);
    }

    public void login(String un, String pw, ResultadoAssincrono delegate) {
        APILoginAsyncTask authTask = new APILoginAsyncTask(delegate);
        authTask.execute(un, pw);
    }

    public void logout() {
        AutenticacaoTokenControlador.getInstance().removeToken();
    }

}
