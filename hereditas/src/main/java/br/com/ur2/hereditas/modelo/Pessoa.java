/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.modelo;

/**
 * Created by mauricio on 24/05/16.
 */
public class Pessoa {
    public String pk;
    public String rfid;
    public String user;
    public String nomecompleto;
    public String login;
    public boolean noServidor = false;

    public boolean Valido()
    {
        return pk != null && user != null && login != null && (pk.length() > 0 && user.length() > 0 && login.length() > 0);
    }

    public Pessoa() { }

    public Pessoa(String k)
    {
        pk = k;
    }
}
