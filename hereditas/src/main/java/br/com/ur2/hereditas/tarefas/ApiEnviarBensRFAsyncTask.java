package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import javax.net.ssl.HttpsURLConnection;

import br.com.ur2.hereditas.ResultadoAssincrono;
import br.com.ur2.hereditas.constantes.API;
import br.com.ur2.hereditas.controlador.BemDadosControlador;
import br.com.ur2.hereditas.modelo.Bem;
import br.com.ur2.hereditas.modelo.RFIDDataTransport;
import br.com.ur2.restproxy.ClienteRESTProxy;
import br.com.ur2.util.Erro;
import br.com.ur2.util.Facilitador;

/**
 * Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
 * Ver arquivo LICENSE em www.hereditas.net.br para detalhes.
 * Created by mauricio on 18/05/16.
 */
public class ApiEnviarBensRFAsyncTask extends AsyncTask<String, Void, Void> {

    String exMsg = "";
    int contagem = 0;
    int total = 0;
    private ResultadoAssincrono delegateResultado = null;

    public ApiEnviarBensRFAsyncTask(ResultadoAssincrono delegate) {
        delegateResultado = delegate;
    }

    @Override
    protected Void doInBackground(String... params) {
        ClienteRESTProxy proxy = new ClienteRESTProxy();

        try {

            proxy.method = ClienteRESTProxy.HttpVerb.PUT;
            proxy.defineToken(params[1],params[2]);

            total = BemDadosControlador.getInstance().obtemListaBensAtualizar().size();

            for (Bem bem: BemDadosControlador.getInstance().obtemListaBensAtualizar()) {

                proxy.defineURL(params[0], API.apiPathBem + bem.pk + "/");

                RFIDDataTransport rfdt = new RFIDDataTransport(bem.rfid);
                proxy.postData = rfdt.Serializar();
                proxy.solicitacao(true, HttpsURLConnection.HTTP_OK);
                contagem ++;
            }
        }
        catch (Exception e)
        {
            Erro.Registra(e);
            exMsg = e.getMessage();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        Facilitador.informa(contagem + " de " + total +  " bens enviados.",true);
        if(exMsg.length() > 0)
        {
            Facilitador.informa(exMsg,true);
        }
        if(contagem > 0 && delegateResultado != null)
            delegateResultado.AoFinalizarTarefaAssincrona();
    }
}
