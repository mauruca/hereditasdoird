/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.constantes;

/**
 * Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
 * Ver arquivo LICENSE em www.hereditas.net.br para detalhes.
 * Created by mauricio on 26/05/16.
 */
public class API {
    public static String baseHttpS = "https://";
    public static String baseHttp = "http://";
    public static String baseURLAPI = ".hereditas.net.br";
    public static String apiPathAuth = "/api-token-auth/";
    public static String apiPathBem = "/api/bem/";
    public static String apiPathLocal = "/api/local/";
    public static String apiPathPessoa = "/api/pessoa/";
    public static String apiPathInventario = "/api/inventario/";
}
