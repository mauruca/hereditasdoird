/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas.controlador;

/**
 * Created by mauricio on 18/05/16.
 */

import java.security.InvalidParameterException;
import java.util.ArrayList;

import br.com.ur2.dispositivo.SenhorDosDispositivos;
import br.com.ur2.dispositivo.modelo.Dispositivo;
import br.com.ur2.dispositivo.modelo.DispositivoRFIDBluetooth;
import br.com.ur2.dispositivo.modelo.Memoria;
import br.com.ur2.dispositivo.modelo.Tag;
import br.com.ur2.hereditas.ResultadoAssincrono;
import br.com.ur2.hereditas.constantes.API;
import br.com.ur2.hereditas.modelo.Bem;
import br.com.ur2.hereditas.modelo.Local;
import br.com.ur2.hereditas.modelo.Token;
import br.com.ur2.hereditas.tarefas.APITestURLAsyncTask;
import br.com.ur2.hereditas.tarefas.ApiBaixarDadosBemAsyncTask;
import br.com.ur2.hereditas.tarefas.ApiBaixarDadosLocalAsyncTask;
import br.com.ur2.hereditas.tarefas.ApiBaixarDadosPessoaAsyncTask;
import br.com.ur2.hereditas.tarefas.ApiEnviarBensRFAsyncTask;
import br.com.ur2.hereditas.tarefas.ApiEnviarInventariosAsyncTask;
import br.com.ur2.hereditas.tarefas.ApiEnviarLocaisRFAsyncTask;
import br.com.ur2.util.Facilitador;

public final class HereditasControlador {

    private static HereditasControlador instance = new HereditasControlador();
    public boolean productionServer = true;
    public Local localSelecionadoEtiquetagem;
    public Bem bemSelecionadoEtiquetagem;
    private Token token = null;
    private String nomeServidor = "";
    private String nomeUsuario = "";
    private final String chaveNomeServidorId = "nomeServidor";
    private final String chaveNomeUsuarioId = "nomeUsuario";
    private final String DispositivoLeitura = "RFID";

    public static HereditasControlador getInstance() {
        return instance;
    }

    public String URL() {
        return montaURL(nomeServidor);
    }

    public void configurar() {
        this.nomeServidor = Facilitador.obterPreferenciaString(chaveNomeServidorId);
        this.nomeUsuario = Facilitador.obterPreferenciaString(chaveNomeUsuarioId);
        usaRFID();
    }

    public boolean usaRFID()
    {
        return obtemDispositivo() instanceof DispositivoRFIDBluetooth;
    }

    public void defineModoInventariar(Boolean rfid) {
        Facilitador.definirPreferencia(DispositivoLeitura, Dispositivo.TipoDispositivo.BarQRCode.toString());
        if(rfid)
        {
            Facilitador.definirPreferencia(DispositivoLeitura, Dispositivo.TipoDispositivo.RFID.toString());
        }
    }

    public void defineNomeServidor(String nomeServidor) throws InvalidParameterException {
        if (nomeServidor == null)
            return;
        nomeServidor = nomeServidor.toLowerCase().trim();
        this.nomeServidor = nomeServidor;
        Facilitador.definirPreferencia(chaveNomeServidorId, nomeServidor);
    }

    public void removeNomeServidor() {
        nomeServidor = "";
        Facilitador.removerPreferencia(chaveNomeServidorId);
    }

    public void verificaURL(ResultadoAssincrono delegate) {
        if (nomeServidor.length() == 0)
            return;
        APITestURLAsyncTask testTask = new APITestURLAsyncTask(delegate);
        testTask.execute(montaURL(this.nomeServidor));
    }

    private String montaURL(String nomeServidor) {
        if (nomeServidor == null || nomeServidor.trim().length() == 0)
            return "";
        nomeServidor = nomeServidor.toLowerCase().trim();
        if (!productionServer) {
            return API.baseHttp.concat(nomeServidor);
        }
        if (!nomeServidor.contains(API.baseURLAPI) && !nomeServidor.contains(API.baseHttpS)) {
            nomeServidor = API.baseHttpS.concat(nomeServidor).concat(API.baseURLAPI);
        }
        return nomeServidor;
    }

    public String obtemNomeServidor() {
        return this.nomeServidor;
    }

    public void defineNomeUsuario(String pNomeUsuario) {
        nomeUsuario = pNomeUsuario;
        Facilitador.definirPreferencia(chaveNomeUsuarioId, pNomeUsuario);
    }

    public void removeNomeUsuario() {
        nomeUsuario = "";
        Facilitador.removerPreferencia(chaveNomeUsuarioId);
    }

    public String obtemNomeUsuario() {
        return nomeUsuario;
    }

    public void baixarDados() {
        // Verifico que tenho token com autorização
        if (!AutenticacaoTokenControlador.getInstance().tokenValido())
            return;
        baixarDadosBens();
        baixarDadosLocais();
        baixarDadosPessoas();
    }

    public void baixarDadosBens() {
        Token token = AutenticacaoTokenControlador.getInstance().obtemToken();
        ApiBaixarDadosBemAsyncTask baixarTask = new ApiBaixarDadosBemAsyncTask();
        baixarTask.execute(URL(), token.nome(), token.apiChave());
    }

    public void baixarDadosLocais() {
        Token token = AutenticacaoTokenControlador.getInstance().obtemToken();
        ApiBaixarDadosLocalAsyncTask baixarTask = new ApiBaixarDadosLocalAsyncTask();
        baixarTask.execute(URL(), token.nome(), token.apiChave());
    }

    public void baixarDadosPessoas() {
        Token token = AutenticacaoTokenControlador.getInstance().obtemToken();
        ApiBaixarDadosPessoaAsyncTask baixarTask = new ApiBaixarDadosPessoaAsyncTask();
        baixarTask.execute(URL(), token.nome(), token.apiChave());
    }

    public void enviarDadosInventario() {
        Token token = AutenticacaoTokenControlador.getInstance().obtemToken();
        ApiEnviarInventariosAsyncTask enviarTask = new ApiEnviarInventariosAsyncTask();
        enviarTask.execute(URL(), token.nome(), token.apiChave());
    }

    public ArrayList<Tag> buscaEtiquetasNaoUsadas(Memoria memoria) {
        ArrayList<Tag> retorno = new ArrayList<Tag>();

        for (Tag tag : memoria.obtemListEtiquetas()) {
            if (BemDadosControlador.getInstance().buscaBemRFID(tag.obtemId()) != null)
                continue;
            if (LocalDadosControlador.getInstance().buscaLocalRFID(tag.obtemId()) != null)
                continue;
            retorno.add(tag);
        }

        return retorno;
    }

    public void enviaDadosLocais() {
        Token token = AutenticacaoTokenControlador.getInstance().obtemToken();
        ApiEnviarLocaisRFAsyncTask enviarTask = new ApiEnviarLocaisRFAsyncTask(new ResultadoAssincrono() {
            @Override
            public void AoFinalizarTarefaAssincrona() {
                baixarDadosLocais();
            }

            @Override
            public void AoFinalizarTarefaAssincrona(Boolean bol) {

            }

            @Override
            public void AoFinalizarTarefaAssincrona(String msg) {

            }
        });
        enviarTask.execute(URL(), token.nome(), token.apiChave());
    }

    public void enviaDadosBens() {
        Token token = AutenticacaoTokenControlador.getInstance().obtemToken();
        ApiEnviarBensRFAsyncTask enviarTask = new ApiEnviarBensRFAsyncTask(new ResultadoAssincrono() {
            @Override
            public void AoFinalizarTarefaAssincrona() {
                baixarDadosBens();
            }

            @Override
            public void AoFinalizarTarefaAssincrona(Boolean bol) {

            }

            @Override
            public void AoFinalizarTarefaAssincrona(String msg) {

            }
        });
        enviarTask.execute(URL(), token.nome(), token.apiChave());
    }

    public Dispositivo obtemDispositivo() {
        return SenhorDosDispositivos.meEmpresta(Facilitador.obterPreferenciaString(DispositivoLeitura));
    }
}
