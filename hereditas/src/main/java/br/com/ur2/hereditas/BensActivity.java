/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Iterator;

import br.com.ur2.hereditas.adaptadores.BemArrayAdapter;
import br.com.ur2.hereditas.controlador.BemDadosControlador;
import br.com.ur2.hereditas.controlador.HereditasControlador;
import br.com.ur2.hereditas.modelo.Bem;
import br.com.ur2.util.Facilitador;

public class BensActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bens);

        configurar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        atualizaLista(BemDadosControlador.getInstance().obtemListaArray());
        if(HereditasControlador.getInstance().obtemDispositivo() != null)
        {
            HereditasControlador.getInstance().obtemDispositivo().parar();
        }
    }

    private void configurar()
    {
        ArrayList<Bem> listabens = new ArrayList<Bem>();

        ListView lv = (ListView) findViewById(R.id.lvListaBensEtiquetar);
        //
        if(lv == null)
            return;

        // crio o adaptador para a lista de locais
        final BemArrayAdapter adapter = new BemArrayAdapter(this,
                android.R.layout.simple_list_item_1, listabens);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selecionaItemLista(parent.getAdapter().getItem(position));
            }
        });

        EditText et = (EditText) findViewById(R.id.etBusca);
    }

    private ArrayList<Bem> filtraLista(CharSequence cs)
    {
        ArrayList<Bem> ret = new ArrayList<Bem>(BemDadosControlador.getInstance().obtemListaArray());
        Iterator<Bem> it = ret.iterator();
        while (it.hasNext()) {
            if (!it.next().descricao.contains(cs)) {
                it.remove();
            }
        }
        return ret;
    }

    private void selecionaItemLista(Object o)
    {
        if(!HereditasControlador.getInstance().obtemDispositivo().ativo())
        {
            ativarDispositivo();
            return;
        }

        if(o == null)
            return;

        HereditasControlador.getInstance().bemSelecionadoEtiquetagem = ((Bem)o);
        Intent intent = new Intent(this, BensDetalheActivity.class);
        startActivity(intent);
    }

    private void ativarDispositivo()
    {
        Facilitador.informa("Dispositivo inativo. Acionando ativação...",true);
        HereditasControlador.getInstance().obtemDispositivo().ativar(this);
    }

    private void atualizaLista(ArrayList<Bem> lista)
    {
        Object o;
        o = findViewById(R.id.lvListaBensEtiquetar);

        if(o == null)
            return;
        ListView lv = (ListView)o;

        ((BemArrayAdapter)lv.getAdapter()).clear();
        ((BemArrayAdapter)lv.getAdapter()).addAll(lista);
        ((BemArrayAdapter)lv.getAdapter()).notifyDataSetChanged();
    }
}
