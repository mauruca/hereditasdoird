/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.hereditas;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.security.InvalidParameterException;

import br.com.ur2.hereditas.controlador.AutenticacaoTokenControlador;
import br.com.ur2.hereditas.controlador.HereditasControlador;
import br.com.ur2.hereditas.controlador.LicencaControlador;
import br.com.ur2.util.Facilitador;

public class ConfigActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        // Ativa eventos de tela
        eventosServidor();
        eventosSerial();
        eventosAutenticacao();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Aplica valores config armazenados
        carregaConfig();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    // Eventos
    private void eventosServidor() {
        Object o;
        o = findViewById(R.id.swServidor);
        if (o != null)
            ((Switch) o).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((Switch) v).isChecked()) {
                        ajustaVisibilidadeNomeServidor();
                        validaDefineNomeEmpresaServidor();
                    } else {
                        HereditasControlador.getInstance().removeNomeServidor();
                        carregaConfig();
                    }
                }
            });
    }

    private void eventosSerial() {
        ((Switch) findViewById(R.id.swLicenca)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (!LicencaControlador.getInstance().licencaVerificada) {
                        validarLicenca();
                    }
                } else {
                    LicencaControlador.getInstance().licencaVerificada = false;
                    LicencaControlador.getInstance().removeSerial();
                    carregaConfig();
                }
            }
        });

        ((EditText) findViewById(R.id.etSerial)).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    validaDefineSerialChave();
            }
        });

        ((EditText) findViewById(R.id.etSerial)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validaDefineSerialChave();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void eventosAutenticacao() {
        Object o;
        o = findViewById(R.id.swAuth);
        if (o != null)
            ((Switch) o).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((Switch) v).isChecked()) {
                        login();
                    } else {
                        logout();
                    }
                }
            });
    }

    // Carrega config
    private void carregaConfig() {
        Object o;
        o = findViewById(R.id.etNomeServidor);
        if (o != null)
            ((EditText) o).setText(HereditasControlador.getInstance().obtemNomeServidor());
        o = findViewById(R.id.etSerial);
        if (o != null)
            ((EditText) o).setText(LicencaControlador.getInstance().obtemSerial());
        o = findViewById(R.id.etChave);
        if (o != null)
            ((EditText) o).setText(LicencaControlador.getInstance().obtemChave());
        o = findViewById(R.id.etLogin);
        if (o != null)
            ((EditText) o).setText(HereditasControlador.getInstance().obtemNomeUsuario());
        ajustaVisibilidadeNomeServidor();
        ajustaVisibilidadeLicenca();
        ajustaVisibilidadeAutenticacao();
        ajustaVisibilidadeModoInventariar();
    }

    private void ajustaVisibilidadeNomeServidor() {
        if (HereditasControlador.getInstance().obtemNomeServidor().length() > 0) {
            if (findViewById(R.id.swServidor) != null)
                ((Switch) findViewById(R.id.swServidor)).setChecked(true);
            if (findViewById(R.id.etNomeServidor) != null)
                (findViewById(R.id.etNomeServidor)).setEnabled(false);
        } else {
            ((Switch) findViewById(R.id.swServidor)).setChecked(false);
            (findViewById(R.id.etNomeServidor)).setEnabled(true);
        }
    }

    private void ajustaVisibilidadeLicenca() {
        if (LicencaControlador.getInstance().licencaVerificada) {
            ((Switch) findViewById(R.id.swLicenca)).setChecked(true);
            ((EditText) findViewById(R.id.etSerial)).setEnabled(false);
            ((EditText) findViewById(R.id.etChave)).setEnabled(false);
        } else {
            ((Switch) findViewById(R.id.swLicenca)).setChecked(false);
            ((EditText) findViewById(R.id.etSerial)).setEnabled(true);
            ((EditText) findViewById(R.id.etChave)).setEnabled(true);
        }
    }

    private void ajustaVisibilidadeAutenticacao() {
        if (AutenticacaoTokenControlador.getInstance().tokenValido()) {
            ((Switch) findViewById(R.id.swAuth)).setChecked(true);
            ((EditText) findViewById(R.id.etLogin)).setEnabled(false);
            ((EditText) findViewById(R.id.etSenha)).setEnabled(false);
            ((EditText) findViewById(R.id.etSenha)).setVisibility(View.INVISIBLE);
        } else {
            ((Switch) findViewById(R.id.swAuth)).setChecked(false);
            ((EditText) findViewById(R.id.etLogin)).setEnabled(true);
            ((EditText) findViewById(R.id.etSenha)).setEnabled(true);
            ((EditText) findViewById(R.id.etSenha)).setVisibility(View.VISIBLE);
        }
    }

    private void ajustaVisibilidadeModoInventariar() {
        if (findViewById(R.id.tbInventariarRFID) != null)
            ((ToggleButton) findViewById(R.id.tbInventariarRFID)).setChecked(HereditasControlador.getInstance().usaRFID());
    }

    //Validações
    private void validaDefineNomeEmpresaServidor() {
        try {
            Object o = findViewById(R.id.etNomeServidor);
            if (o == null)
                return;
            // registra endereço da API
            HereditasControlador.getInstance().defineNomeServidor(((EditText) o).getText().toString().trim());
            // Verifica URL
            HereditasControlador.getInstance().verificaURL(new ResultadoAssincrono() {
                @Override
                public void AoFinalizarTarefaAssincrona() {

                }

                @Override
                public void AoFinalizarTarefaAssincrona(Boolean bol) {
                    Object o;
                    o = findViewById(R.id.swServidor);
                    if (o == null)
                        return;
                    ((Switch) o).setChecked(bol);
                    if (!bol) {
                        toast(getString(R.string.servidorInvalido));
                        HereditasControlador.getInstance().removeNomeServidor();
                    }
                    ajustaVisibilidadeNomeServidor();
                }

                @Override
                public void AoFinalizarTarefaAssincrona(String msg) {

                }
            });
        } catch (InvalidParameterException e) {
            toast(getString(R.string.servidorInvalido));
        }
    }

    private void validaDefineSerialChave() {
        try {
            String serial = ((EditText) findViewById(R.id.etSerial)).getText().toString().trim();
            String chave = "";
            LicencaControlador.getInstance().defineSerial(serial);
            if (LicencaControlador.getInstance().SerialValido()) {
                chave = LicencaControlador.getInstance().obtemChave();
            }
            ((TextView) findViewById(R.id.etChave)).setText(chave);
        } catch (InvalidParameterException e) {
            ((TextView) findViewById(R.id.etChave)).setText("");
        }
    }

    private void validarLicenca() {
        LicencaControlador.getInstance().licenca(new ResultadoAssincrono() {
            @Override
            public void AoFinalizarTarefaAssincrona() {

            }

            @Override
            public void AoFinalizarTarefaAssincrona(Boolean bol) {

            }

            @Override
            public void AoFinalizarTarefaAssincrona(String msg) {
                ajustaVisibilidadeLicenca();
                if (msg.length() > 0) {
                    Facilitador.informa(Facilitador.obtemString(R.string.licencaFalha) + " " + msg, true);
                    return;
                }
                if (!LicencaControlador.getInstance().licencaVerificada) {
                    Facilitador.informa(Facilitador.obtemString(R.string.licencaNExisteInvalida), true);
                }
            }
        });
    }

    private void login() {
        // registra usuário e senha
        String usuario = ((EditText) findViewById(R.id.etLogin)).getText().toString().trim();
        String senha = ((EditText) findViewById(R.id.etSenha)).getText().toString().trim();
        AutenticacaoTokenControlador.getInstance().login(usuario, senha, new ResultadoAssincrono() {
            @Override
            public void AoFinalizarTarefaAssincrona() {
                ajustaVisibilidadeAutenticacao();
                ((EditText) findViewById(R.id.etSenha)).setText("");
            }

            @Override
            public void AoFinalizarTarefaAssincrona(Boolean bol) {

            }

            @Override
            public void AoFinalizarTarefaAssincrona(String msg) {

            }
        });
        HereditasControlador.getInstance().defineNomeUsuario(usuario);
    }

    private void logout() {
        HereditasControlador.getInstance().removeNomeUsuario();
        AutenticacaoTokenControlador.getInstance().logout();
        carregaConfig();
    }

    // onClicks
    public void solicitarLicenca(View view) {
        toast("Será implementado. Clique sobre a chave para copiar e mande um email para autoriza.chave@hereditas.net.br");
    }

    public void copiaChave(View view) {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("chave", ((TextView) findViewById(R.id.etChave)).getText());
        clipboard.setPrimaryClip(clip);
        toast(getString(R.string.copiachave));
    }

    public void alterarModoInventario(View view) {
        HereditasControlador.getInstance().defineModoInventariar(((ToggleButton) view).isChecked());
    }

    // Mensagem Toast
    private void toast(String mensagem) {
        if (mensagem.trim().length() == 0)
            return;
        Toast.makeText(getBaseContext(), mensagem, Toast.LENGTH_SHORT).show();
    }
}
