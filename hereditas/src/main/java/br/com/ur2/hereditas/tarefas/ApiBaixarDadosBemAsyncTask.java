package br.com.ur2.hereditas.tarefas;

import android.os.AsyncTask;

import br.com.ur2.hereditas.constantes.API;
import br.com.ur2.hereditas.controlador.BemDadosControlador;
import br.com.ur2.restproxy.ClienteRESTProxy;
import br.com.ur2.util.Erro;
import br.com.ur2.util.Facilitador;

/**
 * Copyright (C) 2015 Mauricio Costa Pinheiro. Todos os direitos reservados.
 * Ver arquivo LICENSE em www.hereditas.net.br para detalhes.
 * Created by mauricio on 18/05/16.
 */
public class ApiBaixarDadosBemAsyncTask extends AsyncTask<String, Integer, Integer>{

    String exMsg = "";

    @Override
    protected Integer doInBackground(String... params) {

        ClienteRESTProxy proxy = new ClienteRESTProxy();

        try {
            proxy.defineURL(params[0], API.apiPathBem);
            proxy.defineToken(params[1],params[2]);
            BemDadosControlador.getInstance().atualizarListaServidor(proxy.solicitacao(true));
            return BemDadosControlador.getInstance().contagem();
        }
        catch (Exception e)
        {
            Erro.Registra(e);
            exMsg = e.getMessage();
            return 0;
        }
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        if(exMsg.length() > 0)
        {
            Facilitador.informa(exMsg,false);
            return;
        }
        Facilitador.informa("Foram baixados " + integer.toString() + " bens.",true);
    }
}
