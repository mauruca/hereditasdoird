/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo;

import br.com.ur2.dispositivo.BarQRCode.ZXingIntent;
import br.com.ur2.dispositivo.modelo.Dispositivo;
import br.com.ur2.dispositivo.modelo.DispositivoBluetooth;
import br.com.ur2.dispositivo.rfid.DOTR900;

/**
 * Created by mauricio on 26/05/16.
 */
public class SenhorDosDispositivos {

    public static Dispositivo meEmpresta(String tipo)
    {
        if(tipo == null)
            return null;

        switch (tipo)
        {
            case "RFID":
                return DOTR900.Instance();
            case "BarQRCode":
                return ZXingIntent.Instance();
            default:
                return null;
        }
    }

    public static boolean eBluetooth(Dispositivo dispositivo)
    {
        return dispositivo instanceof DispositivoBluetooth;
    }
}
