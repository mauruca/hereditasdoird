/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo.modelo;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by mauricio on 26/05/16.
 */
public class Memoria {

    private Tag _ultima;
    private ArrayList<Tag> _etiquetas = new ArrayList<Tag>();
    private ArrayList<Tag> _etiquetasEntrada = new ArrayList<Tag>();
    private ReentrantReadWriteLock l = new ReentrantReadWriteLock(true);

    public List<Tag> obtemListEtiquetas()
    {
        ArrayList ret;
        l.readLock().lock();
        ret = new ArrayList<Tag>(_etiquetas);
        l.readLock().unlock();
        return ret;
    }

    public void adicionaEtiqueta(String idEtiqueta)
    {
        adicionaEtiqueta(idEtiqueta,0);
    }

    public void adicionaEtiqueta(String idEtiqueta, float sinal)
    {
        if (idEtiqueta == null || idEtiqueta.trim().length() == 0)
            return;

        idEtiqueta = idEtiqueta.trim();

        _ultima = buscaEtiqueta(idEtiqueta);

        // Se nao encontrou cria um novo e adiciona a lista de tags
        if (_ultima == null)
        {
            _ultima = new Tag(idEtiqueta);
            _etiquetasEntrada.add(_ultima);
        }
        _ultima.defineSinal(sinal);
        _ultima.maisUmRegistro();
        l.writeLock().lock();
        _etiquetas = new ArrayList<Tag>(_etiquetasEntrada);
        l.writeLock().unlock();
    }

    public Tag buscaEtiqueta(String tagId)
    {
        for (Tag item: _etiquetasEntrada) {
            if(item.obtemId().equals(tagId))
            {
                return item;
            }
        }
        return null;
    }

    public void limpa()
    {
        limpaUltima();
        limpaTags();
    }
    private void limpaUltima()
    {
        _ultima = null;
    }
    private void limpaTags()
    {
        _etiquetas.clear();
        _etiquetasEntrada.clear();
    }

    public Tag obtemUltimaEtiqueta()
    {
        return _ultima;
    }

    public int totalEtiquetas()
    {
        return obtemListEtiquetas().size();
    }

    @Override
    public boolean equals(Object o) {
        if(!o.getClass().isInstance(this))
            return false;
        Memoria mParam = (Memoria) o;
        return this._ultima == mParam._ultima && this._etiquetas.equals(mParam._etiquetas) && this._etiquetasEntrada.equals(mParam._etiquetasEntrada);
    }
}
