/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo.modelo;

import android.content.BroadcastReceiver;
import android.content.IntentFilter;

/**
 * Created by mauricio on 13/06/16.
 */
public interface DispositivoBluetooth extends Dispositivo {

    IntentFilter obtemFiltrosBluetooth();

    BroadcastReceiver obtemEventosBluetooth();
}
