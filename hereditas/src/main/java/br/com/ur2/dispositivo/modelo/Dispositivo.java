/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo.modelo;

import android.app.Activity;
import android.os.Handler;

/**
 * Created by mauricio on 26/05/16.
 */
public interface Dispositivo {

    public static enum TipoDispositivo
    {
        RFID,
        BarQRCode
    }

    public static enum FormaAtivacao
    {
        ATIVAR_SOMENTE,
        E_CONECTAR,
        CONECTAR_E_LER,
        CONECTAR_E_LER_CONTINUO
    }

    Memoria obtemMemoria();

    void configuraHandlerDispositivo(Handler handlerDispositivo);

    void configuraHandlerDados(Handler handlerDados);

    // Usado no barcode scanner
    Handler obtemHandlerDispositivo();

    void configuraFormaAtivacao(FormaAtivacao forma);

    boolean encontrado();

    boolean ativo();

    boolean conectado();

    void ativar(Activity atividade);

    void desativar();

    void conectar();

    void desconectar();

    void ler(boolean continuous, boolean limpaMemoria);

    void parar();

}
