/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo.modelo;

import android.os.Handler;

/**
 * Created by mauricio on 26/05/16.
 */
public abstract class DispositivoBase  {

    // Implementação feita com handlers devido a thread da interface ser diferente
    // A activity cliente deve implementar o handler tratando os tipos de mensagem
    // que estão na classe MensagemDispositivo
    protected Handler handlerDispositivo;
    protected Handler handlerDados;

    private Memoria memoria = new Memoria();

    public Memoria obtemMemoria()
    {
        return memoria;
    }
}

