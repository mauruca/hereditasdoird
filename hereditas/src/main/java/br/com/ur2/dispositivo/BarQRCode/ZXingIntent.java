/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo.BarQRCode;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import com.google.zxing.integration.android.IntentIntegrator;

import br.com.ur2.dispositivo.modelo.DispositivoBarQRCode;
import br.com.ur2.dispositivo.modelo.DispositivoBase;
import br.com.ur2.dispositivo.modelo.MensagemDispositivo;
import br.com.ur2.util.uLog;

/**
 * Created by mauricio on 13/06/16.
 */
public class ZXingIntent extends DispositivoBase implements DispositivoBarQRCode {

    private static ZXingIntent instance;
    IntentIntegrator integrator;

    public static ZXingIntent Instance() {
        if(instance == null) {
            instance = new ZXingIntent();
        }
        return instance;
    }

    @Override
    public void configuraHandlerDispositivo(Handler handlerDispositivo) {

    }

    @Override
    public void configuraHandlerDados(Handler handlerDados) {
        this.handlerDados = handlerDados;
    }

    @Override
    public Handler obtemHandlerDispositivo() {
        return new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if(msg.what == MensagemDispositivo.aoReceberDados){
                    uLog.Registra("ZXING", MensagemDispositivo.obtemDetalheMensagem(msg));
                    processaPacote(MensagemDispositivo.obtemDetalheMensagem(msg));
                }
            }
        };
    }

    @Override
    public void configuraFormaAtivacao(FormaAtivacao forma) {

    }

    @Override
    public boolean encontrado() {
        return integrator != null;
    }

    @Override
    public boolean ativo() {
        return integrator != null;
    }

    @Override
    public boolean conectado() {
        return integrator != null;
    }

    @Override
    public void ativar(Activity atividade) {

        if(atividade == null)
            return;
        integrator = new IntentIntegrator(atividade);
    }

    @Override
    public void desativar() {
        integrator = null;
    }

    @Override
    public void conectar() {
    }

    @Override
    public void desconectar() {

    }

    @Override
    public void ler(boolean continuous, boolean limpaMemoria) {

        if (limpaMemoria)
        {
            obtemMemoria().limpa();
        }
        integrator.initiateScan();
    }

    @Override
    public void parar() {

    }

    private void processaPacote(String dados)
    {
        obtemMemoria().adicionaEtiqueta(dados);
        MensagemDispositivo.enviaMensagemHandler(handlerDados, MensagemDispositivo.aoReceberDados);
    }

}
