/*
* Copyright (c) 2015-2016 Ultima Ratio Regis Informatica LTDA. Todos direitos reservados
* */

package br.com.ur2.dispositivo.modelo;

/**
 * Created by mauricio on 26/05/16.
 */
public class Tag {

    private String _id = "";
    private int _qtd = 0;
    private float _sinal = 0;

    public Tag() {
    }

    public Tag(String id) {
        defineId(id);
    }

    public String obtemId() {
        return _id;
    }

    public void defineId(String id)
    {
        if (id == null)
        {
            _id = "";
            return;
        }
        _id = id.trim().toUpperCase();
    }

    public float obtemSinal()
    {
        return _sinal;
    }

    public void defineSinal(float valor)
    {
        _sinal = valor;
    }

    public int obtemQuantidade()
    {
        return _qtd;
    }

    public int forcaSinal()
    {
        if(_sinal > -40)
            return 100;
        if(_sinal > -42)
            return 95;
        if(_sinal > -44)
            return 90;
        if(_sinal > -46)
            return 85;
        if(_sinal > -48)
            return 80;
        if(_sinal > -50)
            return 75;
        if(_sinal > -52)
            return 70;
        if(_sinal > -54)
            return 65;
        if(_sinal > -56)
            return 60;
        if(_sinal > -58)
            return 55;
        if(_sinal > -60)
            return 50;
        if(_sinal > -62)
            return 45;
        if(_sinal > -64)
            return 40;
        if(_sinal > -66)
            return 35;
        if(_sinal > -68)
            return 30;
        if(_sinal > -70)
            return 25;
        if(_sinal > -72)
            return 20;
        if(_sinal > -74)
            return 15;
        if(_sinal > -76)
            return 10;
        if(_sinal > -80)
            return 5;
        return 0;
    }

    public boolean valido()
    {
        return _id.length() != 0;
    }

    public void maisUmRegistro()
    {
        _qtd++;
    }

    @Override
    public String toString() {
        return obtemId() + " >> " + obtemQuantidade();
    }

    @Override
    public boolean equals(Object o) {
        return ( (obtemId().equals(((Tag)o).obtemId())));
    }

}
