package com.dotel.libr900;

/************************************************************************************
 * R900Constants revision history                                                   *
 *************+*************+********+***********************************************
 * 2016.06.05	ver 1.0.0  	  mauricio 1. Merged methods from DoTel example files   *
 ************************************************************************************/

public class R900Constants {
    // ---
    private int mTabMode = TAB_LINK;
    public static final int TAB_LINK = 0;
    public static final int TAB_INVENTORY = 1;
    public static final int TAB_ACCESS = 2;
    public static final int TAB_CONFIG = 3;

    private int mAccessType = ACC_TYPE_READ;
    public static final int ACC_TYPE_READ = 0;
    public static final int ACC_TYPE_WRITE = 1;
    public static final int ACC_TYPE_LOCK = 2;
    public static final int ACC_TYPE_KILL = 3;

    // --- Battery Message
    public static final int MSG_BATTERY_CTRL = 50;

    private String mSelTag;
    public static boolean mExit = false;

    public static final int INTENT_MASK = 1;

    // --- Interface Mode
    public static final int MODE_NOT_DETECTED = 0;
    public static final int MODE_BT_INTERFACE = 1;
    public static final int MODE_USB_INTERFACE = 2;


    public static final int LM_KILL_PWD_RW_LOCK = 1 << 9;
    public static final int LM_KILL_PWD_PERM_LOCK = 1 << 8;
    public static final int LM_ACCESS_PWD_RW_LOCK = 1 << 7;
    public static final int LM_ACCESS_PWD_PERM_LOCK = 1 << 6;
    public static final int LM_EPC_MEM_RW_LOCK = 1 << 5;
    public static final int LM_EPC_MEM_PERM_LOCK = 1 << 4;
    public static final int LM_TID_MEM_RW_LOCK = 1 << 3;
    public static final int LM_TID_MEM_PERM_LOCK = 1 << 2;
    public static final int LM_USER_MEM_RW_LOCK = 1 << 1;
    public static final int LM_USER_MEM_PERM_LOCK = 1 << 0;
    public static final int LOCK_PERMA = ( LM_KILL_PWD_PERM_LOCK
            | LM_ACCESS_PWD_PERM_LOCK | LM_EPC_MEM_PERM_LOCK
            | LM_TID_MEM_PERM_LOCK | LM_USER_MEM_PERM_LOCK );
}
