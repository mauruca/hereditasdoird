package com.dotel.libr900;

/***********************************************************************************
* R900Manager revision history                                                     *
*************+*************+********+***********************************************
* 2012.12.12	ver 1.0.0  	  eric     1. Generated(First release)                 *
* 2016.06.05	ver 1.1.0  	  mauricio 2. Merged methods from DoTel example files  *
*										  Removed all direct to user communication *
* 2016.06.13	ver 1.1.1     mauricio 2. Improved socket management               *
* 2016.06.13	ver 1.1.1     mauricio 4. Async messaging to clients               *
*                                         removed OnBtEventListener                *
************************************************************************************/

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import br.com.ur2.util.Erro;


public class R900Manager
{
	public final int REQUEST_ENABLE_BT = 1;

	// ---
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothDevice mBluetoothDevice;
	private BluetoothSocket mBluetoothSocket;
	private UUID mUuid;

	// Events handlers
	private Handler mHandlerNoti;

	private ConnectThread mConnectThread;
	private ConnectedThread mConnectedThread;

	private static final boolean mUseDbgDump = false;
	private FileOutputStream mDbgOutStream;

	private R900RecvPacketParser mPacketParser = new R900RecvPacketParser();

	public R900Manager( Handler handler )
	{
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		mHandlerNoti = handler;

        debugSetup();
	}

    private void debugSetup()
    {
        if( mUseDbgDump )
        {
            try
            {
                String ext = Environment.getExternalStorageState();
                if( ext.equals(Environment.MEDIA_MOUNTED) )
                {
                    String strPath = Environment.getExternalStorageDirectory()
                            .getAbsolutePath();
                    mDbgOutStream = new FileOutputStream(strPath
                            + "/rfid_dbg.txt");
                }
            }
            catch( Exception ex )
            {
                ex.printStackTrace();
            }
        }
    }

    private void debugWrite(byte[] buffer, int bytes)
    {
        if(!mUseDbgDump)
            return;
        try {
            if( mDbgOutStream != null )
                mDbgOutStream.write(buffer, 0, bytes);
        }
        catch(IOException ioe) {
            Log.d("R900Manager","Debug write IO exception");
        }
    }

	private void debugClose()
	{
		if( mDbgOutStream != null )
		{
			try
			{
				mDbgOutStream.close();
			}
			catch( Exception ex )
			{
				ex.printStackTrace();
			}
		}
	}

	public void finalize()
	{
		if( mBluetoothAdapter != null && mBluetoothAdapter.isDiscovering() )
			mBluetoothAdapter.cancelDiscovery();

		disconnect();

		debugClose();
	}

	public final R900RecvPacketParser getRecvPacketParser()
	{
		return mPacketParser;
	}

	public boolean isBluetoothEnabled() {
		return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
	}

	public void enableBluetooth( Activity host )
	{
		if( mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled())
		{
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			host.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}
	}

	public Set<BluetoothDevice> queryPairedDevices()
	{
		if( mBluetoothAdapter != null )
			return mBluetoothAdapter.getBondedDevices();
		return null;
	}

	public void startDiscovery()
	{
        // Garantindo que não iniciamos uma descoberta enquanto estamos conectados
        if(mBluetoothSocket!=null && mBluetoothSocket.isConnected())
            return;
        // Para a descoberta atual
		stopDiscovery();
        // Inicia uma descoberta de dispositivos
		mBluetoothAdapter.startDiscovery();
	}

	public void stopDiscovery()
	{
		if( mBluetoothAdapter != null && mBluetoothAdapter.isDiscovering() )
			mBluetoothAdapter.cancelDiscovery();
	}

	public BluetoothDevice getBluetoothDevice( String address )
	{
		if( mBluetoothAdapter != null )
			return mBluetoothAdapter.getRemoteDevice(address);
		return null;
	}

	public void connectToBluetoothDevice( String address, UUID uuid )
	{
		try
		{
    		final BluetoothDevice DEVICE = getBluetoothDevice(address);
    		if( DEVICE != null )
    			connectToBluetoothDevice(DEVICE, uuid);
		}
		catch( Exception ex )
		{
			ex.printStackTrace();
		}
	}

	public void connectToBluetoothDevice( BluetoothDevice device, UUID uuid )
	{
		mBluetoothDevice = device;
		mUuid = uuid;
		//disconnect();
		mConnectThread = new ConnectThread();
		mConnectThread.start();
	}

	public void disconnect()
	{
        // ----------
        try
        {
            if( mConnectedThread != null )
            {
                mConnectedThread.cancel();
                mConnectedThread.interrupt();
            }
        }
        catch( Exception ex )
        {
            ex.printStackTrace();
        }
        mConnectedThread = null;

		// ----------
		try
		{
			if( mConnectThread != null )
			{
				mConnectThread.cancel();
				mConnectThread.interrupt();
			}
		}
		catch( Exception ex )
		{
			ex.printStackTrace();
		}
		mConnectThread = null;

	}

    // ------------- For Connecting to Bluetooth
    private class ConnectThread extends Thread
    {
        public ConnectThread()
        {
            if( mBluetoothDevice == null ) {
                R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_FAILED_CONNECTED,"ConnectThread - " + "BluetoothDevice is null.");
                return;
            }

            try
            {
                mBluetoothSocket = mBluetoothDevice.createRfcommSocketToServiceRecord(mUuid);
            }
            catch( IOException e )
            {
                R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_FAILED_CONNECTED, "ConnectedThread - " + e.getMessage());
            }
        }

        public void run()
        {
            stopDiscovery();

            if( mBluetoothSocket == null )
            {
                R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_FAILED_CONNECTED, "ConnectedThread - " + "BluetoothSocket is null.");
                return;
            }

            try
            {
                if(mBluetoothSocket.isConnected())
                    mBluetoothSocket.close();
                mBluetoothSocket.connect();
            }
            catch( IOException connectException )
            {
                try
                {
                    mBluetoothSocket.close();
                }
                catch( IOException closeException )
                {
                    Log.d("R900Manager","ConnectThread.run closing socket " + closeException.getMessage());
                    R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_FAILED_CONNECTED, "ConnectedThread - " + "run closing socket " + closeException.getMessage());
                }
                R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_FAILED_CONNECTED, "ConnectThread - " + "run connecting to socket " + connectException.getMessage());
                mConnectThread = null;
                return;
            }

            // manageConnectedSocket(mmSocket);
            mConnectedThread = new ConnectedThread(mBluetoothSocket);
            if( mConnectedThread.isConnected() )
            {
                mConnectedThread.start();

                R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_CONNECTED, "Device " + mBluetoothDevice.getName());
            }


            mConnectThread = null;
        }


        public void cancel()
        {
            try {
                if(mBluetoothSocket.isConnected())
                    mBluetoothSocket.close();

            } catch (IOException e) {
                Erro.Registra(e);
            }
        }
    };

	// ------------- For Manage bluetooth
	private class ConnectedThread extends Thread
	{
		private final InputStream mmInStream;
		private final OutputStream mmOutStream;
		private boolean valid;
		
		public ConnectedThread( BluetoothSocket socket )
		{
			InputStream tmpIn = null;
			OutputStream tmpOut = null;
			mPacketParser.reset();
			try
			{
				tmpIn = socket.getInputStream();
				tmpOut = socket.getOutputStream();
			}
			catch( IOException e )
			{
				tmpIn = null;
				tmpOut = null;
                R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_FAILED_CONNECTED, "ConnectedThread - " + e.getMessage());
			}
			mmInStream = tmpIn;
			mmOutStream = tmpOut;
		}

		public boolean isConnected()
		{
    		return mmInStream != null && mmOutStream != null;
		}
		
		public void run()
		{
			byte[] buffer = new byte[1024];
			int bytes;
			while( true )
			{
				try
				{
					bytes = mmInStream.read(buffer);

                    debugWrite(buffer,bytes);

					mPacketParser.pushPacket(buffer, bytes);

                    // Notify data received
                    R900Messages.notify(mHandlerNoti,R900Messages.MSG_BT_DATA_RECV);
				}
				catch( IOException e )
				{
                    R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_DATA_EXCEPTION, "[Bluetooth Socket] Read Fail : "+ e.getMessage());
					break;
				}
			}
		}

		/* Call this from the main Activity to send data to the remote device */
		public void write( byte[] bytes )
		{
			try
			{
				mmOutStream.write(bytes);
                // Notify send data
                R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_DATA_SENT, bytes);
			}
			catch( IOException e )
			{
                R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_DATA_EXCEPTION, "[Bluetooth Socket] Write Fail : " + e.getMessage());
			}
		}

		/* Call this from the main Activity to shutdown the connection */
		public void cancel()
		{
			try
			{
				mmInStream.close();
				mmOutStream.close();
			}
			catch( IOException e )
			{
                Erro.Registra(e);
			}
		}
	}

	public void sendData( byte[] bytes )
	{
		if( mConnectedThread != null )
		{
			//Log.d(BluetoothActivity.TAG, "Send : " + new String(bytes));
			mConnectedThread.write(bytes);
		}
		else
		{
            // Notify data transport exception
            R900Messages.notify(mHandlerNoti, R900Messages.MSG_BT_DATA_EXCEPTION, "[Bluetooth Socket] Write Fail : Bluetooth Thread is not running.");
		}
	}

	public boolean isTryingConnect()
	{
		return mConnectThread != null;
	}

	public boolean isConnected()
	{
		if(mBluetoothSocket == null)
			return false;
		return mBluetoothSocket.isConnected();
	}

    public void sendInventoryReportingFormat( int f_time, int f_rssi )
    {
        sendData(R900Protocol.makeProtocol( R900Protocol.CMD_INVENT_REPORT_FORMAT,
                    new int[]{ f_time, f_rssi } ) );
    }
}
