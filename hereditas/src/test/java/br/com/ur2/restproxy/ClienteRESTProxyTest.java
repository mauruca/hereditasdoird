package br.com.ur2.restproxy;

/**
 * Created by mauricio on 17/06/16.
 */

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.security.InvalidParameterException;

import static org.junit.Assert.*;

public class ClienteRESTProxyTest {

    @Rule
    public ExpectedException ex = ExpectedException.none();

    @Test
    public void defineURL_ParametrosNulos() throws MalformedURLException {
        ClienteRESTProxy cp = new ClienteRESTProxy();
        ex.expect(MalformedURLException.class);
        cp.defineURL(null,null);
    }

    @Test
    public void defineURL_ParametrosVazios() throws MalformedURLException {
        ClienteRESTProxy cp = new ClienteRESTProxy();
        ex.expect(MalformedURLException.class);
        cp.defineURL("","");
    }

    @Test
    public void defineURL_EnderecoNulo() throws MalformedURLException {
        ClienteRESTProxy cp = new ClienteRESTProxy();
        ex.expect(MalformedURLException.class);
        cp.defineURL(null,"teste");
    }

    @Test
    public void defineURL_EnderecoSemProtocolo() throws MalformedURLException {
        ClienteRESTProxy cp = new ClienteRESTProxy();
        ex.expect(MalformedURLException.class);
        ex.expectMessage("no protocol");
        cp.defineURL("www.google.com",null);
    }

    @Test
    public void defineURL_ParametrosNulosConstrutor() throws MalformedURLException {
        ex.expect(MalformedURLException.class);
        ClienteRESTProxy cp = new ClienteRESTProxy(null,null);
    }

    @Test
    public void defineToken_ParametrosNulos() throws InvalidParameterException, IOException {
        ClienteRESTProxy cp = new ClienteRESTProxy();
        cp.defineToken(null,null);
        ex.expect(InvalidParameterException.class);
        ex.expectMessage("Token solicitado não foi informado.");
        cp.solicitacao(true);
    }

    @Test
    public void defineToken_ParametrosVazios() throws InvalidParameterException, IOException {
        ClienteRESTProxy cp = new ClienteRESTProxy();
        cp.defineToken("","");
        ex.expect(InvalidParameterException.class);
        ex.expectMessage("Token solicitado não foi informado.");
        cp.solicitacao(true);
    }

    @Test
    public void defineToken_TokenVazioConstrutor() throws InvalidParameterException, IOException {
        ClienteRESTProxy cp = new ClienteRESTProxy();
        ex.expect(InvalidParameterException.class);
        ex.expectMessage("Token solicitado não foi informado.");
        cp.solicitacao(true);
    }

    @Test
    public void defineToken_TokenVazioComURL() throws InvalidParameterException, IOException {
        ClienteRESTProxy cp = new ClienteRESTProxy("http://jsonplaceholder.typicode.com","/posts");
        ex.expect(InvalidParameterException.class);
        ex.expectMessage("Token solicitado não foi informado.");
        cp.solicitacao(true);
    }

    @Test
    public void solicitacao_ConstrutorVazio() throws InvalidParameterException, IOException {
        ClienteRESTProxy cp = new ClienteRESTProxy();
        ex.expect(InvalidParameterException.class);
        ex.expectMessage("URL não informada.");
        cp.solicitacao(false);
    }

}
