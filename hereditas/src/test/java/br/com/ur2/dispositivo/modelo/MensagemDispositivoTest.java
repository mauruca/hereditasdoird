package br.com.ur2.dispositivo.modelo;

/**
 * Created by mauricio on 17/06/16.
 */

import android.os.Handler;
import android.os.Message;

import org.junit.Test;

import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.*;

public class MensagemDispositivoTest {

    @Rule
    public ExpectedException ex = ExpectedException.none();

    private int idAtual = 0;
    private String mensagemAtual = "";


    private Handler obtemHandlerMock()
    {
        final Handler handler = mock(Handler.class);
        when(handler.sendMessageAtTime(any(Message.class), anyLong())).thenAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                //Message msg = invocation.getArgumentAt(0, Message.class);
                Message msg = (Message) invocation.getArguments()[0];
                msg.getCallback().run();
                return null;
            }
        });
        return handler;
    }

    @Test
    public void enviaMensagemIdInvalido()
    {
        ex.expect(IllegalArgumentException.class);
        MensagemDispositivo.enviaMensagemHandler(new Handler(),123);
    }


}
