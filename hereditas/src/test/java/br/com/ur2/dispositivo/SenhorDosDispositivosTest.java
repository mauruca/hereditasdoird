package br.com.ur2.dispositivo;

/**
 * Created by mauricio on 17/06/16.
 */

import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.rules.ExpectedException;

import br.com.ur2.dispositivo.modelo.Dispositivo;

public class SenhorDosDispositivosTest {

    @Rule
    public ExpectedException ex = ExpectedException.none();

    @Test
    public void meEmpresaTipoNulo()
    {
        Dispositivo dispositivoEsperado = null;
        Dispositivo dispositivoAtual = SenhorDosDispositivos.meEmpresta("");
        assertEquals(dispositivoEsperado, dispositivoAtual);
    }


    @Test
    public void meEmpresaTipoNaoBluetooth()
    {
        Dispositivo dispositivoAtual = SenhorDosDispositivos.meEmpresta(Dispositivo.TipoDispositivo.BarQRCode.toString());
        assertFalse(SenhorDosDispositivos.eBluetooth(dispositivoAtual));
    }
}
